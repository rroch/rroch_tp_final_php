<?php

namespace src\Model;

class Avis
{
    private $idAvis;
    private $auteur;
    private $note;
    private $commentaire;

    /**
     * Constructeur de l'objet Avis
     * @param int $idAvis
     * @param string $auteur
     * @param int $note
     * @param string $commentaire
     */
    public function __construct(int $idAvis, string $auteur, int $note, string $commentaire)
    {
        $this->idAvis = $idAvis;
        $this->auteur = $auteur;
        $this->note = $note;
        $this->commentaire = $commentaire;
    }

    /**
     * @return int
     */
    public function getIdAvis(): int
    {
        return $this->idAvis;
    }

    /**
     * @param int $idAvis
     */
    public function setIdAvis(int $idAvis)
    {
        $this->idAvis = $idAvis;
    }

    /**
     * @return string
     */
    public function getAuteur(): string
    {
        return $this->auteur;
    }

    /**
     * @param string $auteur
     */
    public function setAuteur(string $auteur)
    {
        $this->auteur = $auteur;
    }

    /**
     * @return int
     */
    public function getNote(): int
    {
        return $this->note;
    }

    /**
     * @param int $note
     */
    public function setNote(int $note)
    {
        $this->note = $note;
    }

    /**
     * @return string
     */
    public function getCommentaire(): string
    {
        return $this->commentaire;
    }

    /**
     * @param string $commentaire
     */
    public function setCommentaire(string $commentaire)
    {
        $this->commentaire = $commentaire;
    }
}