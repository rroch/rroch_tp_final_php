<?php

namespace src\Model;

class Restaurants
{
    private $idRestaurant;
    private $nom;
    private $adresse;
    private $cp;
    private $ville;
    private $telephone;
    private $description;
    private $mesAvis;

    /**
     * Constructeur de l'objet Restaurant
     * @param int $idRestaurant
     * @param string $nom
     * @param string $adresse
     * @param int $cp
     * @param string $ville
     * @param int $telephone
     * @param string $description
     * @param array $mesAvis
     */
    public function __construct(int $idRestaurant, string $nom, string $adresse, int $cp, string $ville, int $telephone, string $description, array $mesAvis)
    {
        $this->idRestaurant = $idRestaurant;
        $this->nom = $nom;
        $this->adresse = $adresse;
        $this->cp = $cp;
        $this->ville = $ville;
        $this->telephone = $telephone;
        $this->description = $description;
        $this->mesAvis = $mesAvis;
    }

    /**
     * @return int
     */
    public function getIdRestaurant(): int
    {
        return $this->idRestaurant;
    }

    /**
     * @param int $idRestaurant
     */
    public function setIdRestaurant(int $idRestaurant)
    {
        $this->idRestaurant = $idRestaurant;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getAdresse(): string
    {
        return $this->adresse;
    }

    /**
     * @param string $adresse
     */
    public function setAdresse(string $adresse)
    {
        $this->adresse = $adresse;
    }

    /**
     * @return int
     */
    public function getCp(): int
    {
        return $this->cp;
    }

    /**
     * @param int $cp
     */
    public function setCp(int $cp)
    {
        $this->cp = $cp;
    }

    /**
     * @return string
     */
    public function getVille(): string
    {
        return $this->ville;
    }

    /**
     * @param string $ville
     */
    public function setVille(string $ville)
    {
        $this->ville = $ville;
    }

    /**
     * @return int
     */
    public function getTelephone(): int
    {
        return $this->telephone;
    }

    /**
     * @param int $telephone
     */
    public function setTelephone(int $telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return array
     */
    public function getMesAvis(): array
    {
        return $this->mesAvis;
    }

    /**
     * @param array $mesAvis
     */
    public function setMesAvis(array $mesAvis)
    {
        $this->mesAvis = $mesAvis;
    }

}