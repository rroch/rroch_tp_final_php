<?php

namespace src\Model\DAO;

require_once __DIR__ . "/../Avis.php";
require_once __DIR__ . "/../../Utils/MyPDO.php";

use Exception;
use src\Model\Avis;
use src\Utils\MyPDO;


class AvisDAO
{

    /**
     * Fonction permettant de récupérer un avis à partir de son id
     * @param int $idAvis Id de l'avis à récupérer
     * @return Avis Objet Avis récupéré
     */
    public static function getOneAvis(int $idAvis): Avis
    {
        try {
            $myPDO = new MyPDO();
            $res = $myPDO->getMyPDO()->query("SELECT * FROM avis WHERE idAvis = " . $idAvis)->fetchAll()[0];
            unset($myPDO);
            var_dump($res);
            return new Avis($res['idAvis'], $res['auteur'], $res['note'], $res['commentaire']);

        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
            die();
        }
    }

    /**
     * Fonction permettant de récupérer tous les avis de la base de données
     * @return array
     */
    public static function getAllAvis(): array
    {
        $myPDO = new MyPDO();
        $res = $myPDO->getMyPDO()->query("SELECT * FROM avis")->fetchAll();
        unset($myPDO);
        $allAvis = [];
        foreach ($res as $avis) {
            $allAvis[] = new Avis($avis['idAvis'], $avis['auteur'], $avis['note'], $avis['commentaire']);
        }

        return $allAvis;
    }

    /**
     * Fonction permettant de récupérer tous les avis de la base de données
     * @return array
     */
    public static function getAllAvisByRestaurants(int $idRestaurant): array
    {
        $myPDO = new MyPDO();
        $res = $myPDO->getMyPDO()->query("SELECT * FROM avis WHERE idRestaurant = " . $idRestaurant)->fetchAll();
        unset($myPDO);
        $allAvis = [];
        foreach ($res as $avis) {
            $allAvis[] = new Avis($avis['idAvis'], $avis['auteur'], $avis['note'], $avis['commentaire']);
        }

        return $allAvis;
    }

}