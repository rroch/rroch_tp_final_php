<?php

namespace src\Model\DAO;

require_once __DIR__ . "/../Restaurants.php";
require_once __DIR__ . "/../../Utils/MyPDO.php";

use Exception;
use src\Model\Restaurants;
use src\Utils\MyPDO;

class RestaurantsDAO
{

    public static function getOneRestaurant(int $idRestaurant)
    {
        try {
            $myPDO = new MyPDO();
            $res = $myPDO->getMyPDO()->query("SELECT * FROM restaurants WHERE idRestaurant = " . $idRestaurant)->fetchAll()[0];
            unset($myPDO);
            return new Restaurants($res["idRestaurant"], $res["nom"], $res["adresse"], $res["cp"], $res["ville"], $res["telephone"], $res["description"],AvisDAO::getAllAvisByRestaurants($res["idRestaurant"]));

        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
            die();
        }
    }

    public static function getAllRestaurants(): array
    {
        $myPDO = new MyPDO();
        $res = $myPDO->getMyPDO()->query("SELECT * FROM restaurants")->fetchAll();
        unset($myPDO);
        $allRestaurants = [];
        foreach ($res as $restaurant) {
            $allRestaurants[] = new Restaurants($restaurant["idRestaurant"], $restaurant["nom"], $restaurant["adresse"], $restaurant["cp"], $restaurant["ville"], $restaurant["telephone"], $restaurant["description"],AvisDAO::getAllAvisByRestaurants($restaurant["idRestaurant"]));
        }
        return $allRestaurants;
    }


}