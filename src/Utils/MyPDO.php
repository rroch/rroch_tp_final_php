<?php

namespace src\Utils;

use Exception;
use PDO;

class MyPDO
{
    private $myPDO;

    /**
     * Constructeur de l'objet MyPDO
     */
    public function __construct()
    {
        try {
            $hostname = "127.0.0.1";
            $username = "root";
            $password = "";
            $dbname = "restaurant";

            $this->myPDO = new PDO('mysql:host=' . $hostname . ';dbname=' . $dbname . ';charset=utf8mb4', $username, $password);
        } catch (Exception $e) {
            echo "Erreur Création PDO : " . $e->getMessage();
            die();
        }

    }

    /**
     * @return PDO
     */
    public function getMyPDO(): PDO
    {
        return $this->myPDO;
    }


}