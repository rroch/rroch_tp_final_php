<?php

use src\Model\DAO\RestaurantsDAO;

require_once __DIR__ . '/src/Model/DAO/AvisDAO.php';
require_once __DIR__ . '/src/Model/DAO/RestaurantsDAO.php';

$allRestaurants = RestaurantsDAO::getAllRestaurants();

?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mes Restaurants</title>
</head>
<body>
<p><a href="index.php">Restaurants préférés</a> <a href="#">Contact</a></p>
<h1>Vos restaurants préférés : </h1>
<?php

foreach ($allRestaurants as $restaurant) {
    echo '<h2><a href="./restaurant.php?idRestaurant=' . $restaurant->getIdRestaurant() . '">' . $restaurant->getNom() . '</a> </h2>
        <i>' . $restaurant->getVille() . '</i>
        <p>' . $restaurant->getDescription() . '</p>
        <hr>
        ';
}

?>

</body>
</html>

