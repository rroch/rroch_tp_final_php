<?php

use src\Model\DAO\RestaurantsDAO;

require_once __DIR__ . '/src/Model/DAO/AvisDAO.php';
require_once __DIR__ . '/src/Model/DAO/RestaurantsDAO.php';

$restaurant = RestaurantsDAO::getOneRestaurant($_GET['idRestaurant']);

?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Détail <?= $restaurant->getNom() ?></title>
</head>
<body>
<p><a href="index.php">Restaurants préférés</a> <a href="#">Contact</a></p>
<?php
echo '<h1>' . $restaurant->getNom() . '</h1>
     <p>' . $restaurant->getAdresse() . '<br>' . $restaurant->getCp() . ' ' . $restaurant->getVille() . '</p>
     <h2>Description</h2>
     <p>' . $restaurant->getDescription() . '</p>
     <h2>Avis</h2>';
foreach ($restaurant->getMesAvis() as $avis) {
    $starHTML = "<p style='font-size: 20px; margin: 0'>" . $avis->getAuteur() . " : ";
    for ($i = 0; $i < $avis->getNote(); $i++) {
        $starHTML .= "&#9733;";
    }
    for ($i = 0; $i < 5 - $avis->getNote(); $i++) {
        $starHTML .= "&#9734;";
    }
    $starHTML .= "</p>";
    echo $starHTML;
    echo "<p>" . $avis->getCommentaire() . "</p><br>";
}


?>

</body>
</html>
